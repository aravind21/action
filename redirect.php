<?php
require 'config.php';

// Get the config data from config.ini       
$conf_obj = new config('config.ini');
$config = $conf_obj->getAllConfig();

$currentUrl = parse_url($_SERVER['REQUEST_URI']);

// Keep URL Parameters
$redirect_path = $config[GeneralSetting][KeepURLParameter] ? $config[GeneralSetting][Redirect][RedirectPath] . $currentUrl["query"] : $config[GeneralSetting][Redirect][RedirectPath];
// Redirect Time
$redirect_time = $config[GeneralSetting][Redirect][RedirectTime];

// Webgains
$webgains = $_GET['trafficsource'] === "webgains" ? "true" : "false";
$lead_reference = empty($_GET['lead_reference']) ? " " : $_GET['lead_reference'];

?>
<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">  
<?php if ($config[GeneralSetting][Redirect][RedirectStatus]) { ?>  
            <meta http-equiv="refresh" content="<?php echo $redirect_time; ?>;url=<?php echo $redirect_path; ?>" />  
        <?php } ?>

        <title>Wellness mit StimaWELL&reg; FaceTime - Jetzt kaufen!</title>

        <!-- implementation bootstrap -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- implementation fontawesome icons -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- implementation simpleline icons -->
        <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
        <!-- implementation googlefonts -->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
        <!-- implementation Animated Header -->
        <!-- implementation custom css -->
        <link href="css/creative.css" rel="stylesheet">
        <!-- implementation animate css -->
        <link href="css/animate.css" rel="stylesheet">
        
        <!-- Google Analytics -->
        <?php include_once 'clients/tracking/google/google.php'; ?>        

        <!-- Facebook Pixel -->
        <?php include_once('clients/tracking/facebook/facebook.php'); ?>          
        
    </head>


    <!-- Outbrain Tracking -->
    <?php $config[TrackingTools][EnableOutbrain] ? include_once'clients/tracking/outbrain/outbrain_redirect.php' : ' '; ?>

    <body>
        <!--Adeblo Tracking-->
        <?php $config[TrackingTools][EnableAdeblo] ? include_once'clients/tracking/adeblo/adeblo.php' : ' ' ?>
        
        <!-- Remarketing Target360 Tracking -->
        <?php $config[TrackingTools][EnableTarget360] ? include_once 'clients/tracking/target360/target_redirect.php' : ' ' ?>;
        
        <p>&nbsp;</p>
        <div class="col-sm-12 text-center logo">
            <img class="img-fluid" alt="StimaWELL FaceTime" src="img/facetime-logo.png">
        </div>

        <!-- DESKTOP NAV -->
        <nav class="ft-nav">   
            <div class="container">   
                <div class="col-sm-12 text-center">
                    <a href="https://online-mehr-geschaeft.de/stimawell/facetime-neu/" class="js-btn" data-hover="home">stimawell&reg;</a>
                    <a href="#ueber" class="js-btn" data-hover="snippets">über FaceTime</a>
                    <a href="#anwendung" class="js-btn" data-hover="stats">anwendung</a>
                    <a href="#refs" class="js-btn" data-hover="stats">referenzen</a>
                    <a href="#buy" class="js-btn" data-hover="customers">jetzt kaufen</a> 
                </div>
            </div>
        </nav>

        <!-- MOBILE IMG --> 
        <div class="facetime-img-mobile col-sm-12 text-center">
            <img class="img-fluid" src="img/facetime-model.png"/>   
        </div>

        <!-- HERO HEADER -->
        <section class="hero">
            <div class="container">
                <div class="row">




                    <div class="hero-header col-lg-12 col-sm-12">
                        <div class="hero-intro">
                            <p>&nbsp;</p>
                            <center>
                                <h3>BITTE ÜBERPRÜFE DEIN E-MAIL POSTFACH.</h3> 
                                <hr> 
                                <p style="font-size: 18px;">Wir haben dir eine E-Mail mit einem Bestätigungslink an deine genannte E-Mail-Adresse gesendet. Solltest du in den nächsten 15 Minuten keine Mail erhalten, schaue bitte auch in dein Spam oder Junk Ordner.</p>
                            </center>
                            <p>&nbsp;</p>

                        </div>  
                    </div> 

                </div>
            </div>         
        </section>  
        <!-- ./HERO HEADER -->  



        <footer class="footer text-center">
            <div class="container">
                <div class="row">


                    <div class="col-lg-12 h-100 text-center my-auto">


                        <nav class="navbar">   
                            <div class="container">
                                <div class="col-sm-12 text-center">

                                    <a href="https://www.stimawell-ems.de/" class="js-btn" data-hover="home">Shop</a>
                                    <a href="https://www.stimawell-ems.de/impressum" class="js-btn" data-hover="home">Impressum</a>
                                    <a href="https://www.stimawell-ems.de/agb" class="js-btn" data-hover="snippets">AGB</a>
                                    <a href="https://www.stimawell-ems.de/datenschutz" class="js-btn" data-hover="features">Datenschutz</a>

                                </div>
                            </div>
                        </nav>





 <!--<p class="text-muted small mb-4 mb-lg-0"><strong>✔</strong> 40 JAHRE MADE IN GERMANY&nbsp;&nbsp;&nbsp;<strong>✔</strong> DIREKT VOM HERSTELLER&nbsp;&nbsp;&nbsp;<strong>✔</strong> SERVICE: 06443 4369914</p>-->


                        <p>&nbsp;</p>

                        <ul class="list-inline mb-0">
                            <li class="list-inline-item mr-3">
                                <a href="https://www.facebook.com/stimawellems/" target="_blank">
                                    <i class="fa fa-facebook fa-2x fa-fw"></i>
                                </a>
                            </li>
                            <li class="list-inline-item mr-3">
                                <a href="https://www.instagram.com/stimawell.ems/" target="_blank">
                                    <i class="fa fa-instagram fa-2x fa-fw"></i>
                                </a>
                            </li>
                        </ul>


                    </div> 
                </div>   
            </div>
        </footer>

        <!-- </Webgains Tracking Code> -->    
        <?php $config[TrackingTools][EnableWebGains] ? include_once 'clients/tracking/webgains/webgains_redirect.php' : ' '; ?>     


        <!-- Bootstrap core JavaScript -->
        <script src="vendor/jquery/jquery.min.js">
        </script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js">
        </script>
        <script src="vendor/header-animation/demo-1.js">
        </script>  
        <script src="vendor/header-animation/TweenLite.min.js">
        </script>
        <script src="vendor/header-animation/EasePack.min.js">
        </script>
        <script src="vendor/header-animation/rAF.js">
        </script>
        <script src="vendor/header-animation/demo-1.js">
        </script>  

    </body>

</html>