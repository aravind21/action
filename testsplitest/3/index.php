<?php
require "cio.php";

# Facebook Pixel URL
$facebookpixel = $config[TrackingTools][FacebookPixel];
$fbURL = "https://www.facebook.com/tr?id=" . $config[TrackingTools][FacebookPixel] . "&ev=PageView&noscript=1";

# Google Maps URL
$googleKey = $config[TrackingTools][GoogleAPIKey];
$googleURL = "https://maps.googleapis.com/maps/api/js?key=" . $config[TrackingTools][GoogleAPIKey] . "&libraries=places";

#Google Analytics Key
$googleAnalyticsKey = $config[TrackingTools][GoogleAnalytics];
$googleAnalyticsURL = "https://www.googletagmanager.com/gtag/js?id=" . $config[TrackingTools][GoogleAnalytics];
?>

<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Nutzen Sie unser neues FREE EMS® PROGRAMM und trainieren sie bequem von zu Hause aus.">
        <meta name="author" content="StimaWELL">

        <title>Campaign Theame</title>

        <link href="css/creative.css" rel="stylesheet" type="text/css">  
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">    
        <link rel="stylesheet" href="vendor/telefonvalidator-client/build/css/intlTelInput.css"/>
        <link rel="stylesheet" href="vendor/bxslider/src/css/jquery.bxslider.css"/>
        <link rel="stylesheet" href="css/animate.css"/>
    </head>

    <script type="text/javascript" src="vendor/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="vendor/telefonvalidator-client/build/js/utils.js"></script>
    <script type="text/javascript" src="vendor/telefonvalidator-client/build/js/intlTelInput.js"></script>
    <script type="text/javascript" src="vendor/bxslider/src/js/jquery.bxslider.js "></script>
    <script type="text/javascript" src="vendor/google/maps.js"></script>
    <script type="text/javascript" src="vendor/jquery-cookie-master/src/jquery.cookie.js"></script>
    <script type="text/javascript">
        var countdown_status = "<?= $counter_status; ?>";
        var countdown_expire = "<?= $config[GeneralSetting][CountdownExpire]; ?>";
        var current_stocks = "<?= $current_count_value; ?>";
        var autofill_postal_code = "<?= $config[Postal_Code][Autofill]; ?>";
        var localization_postal_code = "<?= $config[Postal_Code][Localization]; ?>"
        var countdown_expire_message = "<?= $config[GeneralSetting][CountdownExpireMessage] ?>";
        var validate_telefon = "<?= $config[Telefon][Status]; ?>";
        var single_submit_text = "<?= $single_submit_text ?>"
        var cookie = "<?= $cookie; ?>";
        var fb_events = "<?= $config[TrackingTools][FacebookEvent] ?>";
        var fb_event = (fb_events.split(",")); // Split the events to array
        var fb_curreny = "<?= $config[TrackingTools][FacebookCurrency] ?>";
        var fb_value = "<?= $config[TrackingTools][FacebookValue] ?>";
    </script>
    <script language="JavaScript" type="text/javascript" src="cio.js"></script>   	

    <script type="text/javascript" src="<?php echo $googleURL; ?>"></script>   	
    <script async src="<?php echo $googleAnalyticsURL; ?>"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', '<?php echo $googleAnalyticsKey; ?>');
    </script>	

    <script>
        /*Get the facebook pixel code from config.ini */
        var facebookpixel = <?= $facebookpixel ?>;
        !function (f, b, e, v, n, t, s)
        {
            if (f.fbq)
                return;
            n = f.fbq = function () {
                n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq)
                f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
                'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', <?= $facebookpixel ?>);
        fbq('track', 'PageView');
        fb_event.forEach(function (e) {
            e = e.trim();
            // Purchace event needs currency and amount value 
            if (e == "Purchase") {
                var s = 'track';
                s += e
                s += {currency: fb_curreny, value: fb_value}
                console.log(s)
                fbq('track', e, {currency: fb_curreny, value: fb_value});
            } else {
                fbq('track', e);
            }


        })


    </script>
    <noscript>
    <img height="1" width="1" style="display:none" src= "<?php echo $fbURL ?>"/>
    </noscript>	
    
    <!-- Outbrain Tracking -->
    <?php if ($config[TrackingTools][EnableOutbrain]) { ?>
        <script data-obct type="text/javascript">
            /** DO NOT MODIFY THIS CODE**/
            !function (_window, _document) {
                var OB_ADV_ID = '007035eac06ecc119b917903a5dc025178';
                if (_window.obApi) {
                    var toArray = function (object) {
                        return Object.prototype.toString.call(object) === '[object Array]' ? object : [object];
                    };
                    _window.obApi.marketerId = toArray(_window.obApi.marketerId).concat(toArray(OB_ADV_ID));
                    return;
                }
                var api = _window.obApi = function () {
                    api.dispatch ? api.dispatch.apply(api, arguments) : api.queue.push(arguments);
                };
                api.version = '1.1';
                api.loaded = true;
                api.marketerId = OB_ADV_ID;
                api.queue = [];
                var tag = _document.createElement('script');
                tag.async = true;
                tag.src = '//amplify.outbrain.com/cp/obtp.js';
                tag.type = 'text/javascript';
                var script = _document.getElementsByTagName('script')[0];
                script.parentNode.insertBefore(tag, script);
            }(window, document);
            obApi('track', 'PAGE_VIEW');
        </script>    
    <?php } ?>
    <!-- End of Outbrain Tracking -->

    <body>
        <!-- Web Gains Tracking -->
        <?php if ($config[TrackingTools][EnableWebGains]) { ?>
            <!--Web Tracking-->
            <script type="text/javascript">
                (function (w, e, b, g, a, i, n, s) {
                    w['ITCLKOBJ'] = a;
                    w[a] = w[a] || function () {
                        (w[a].q = w[a].q || []).push(arguments)}, w[a].l = 1 * new Date();
                    i = e.createElement(b), n = e.getElementsByTagName(b)[0];
                    i.async = 1;
                    i.src = g;
                    n.parentNode.insertBefore(i, n)
                })(window, document, 'script', 'https://analytics.webgains.io/clk.min.js', 'ITCLKQ');
                ITCLKQ('set', 'internal.cookie', true);
                ITCLKQ('click');
            </script> 
        <?php } ?>
        <!-- End of Web Gains Tracking -->
        
        <!-- COUNTDOWN -->
        <section>
            <div class="nav-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 text-center">

                            <span class="countdown-nav">Aktionsende:&nbsp;&nbsp;</span><span class="countdown-nav" id="demo"></span>&nbsp;<span class="countdown-nav">-&nbsp;&nbsp;Nur noch <?php echo $current_count_value; ?> von <?php echo $config[GeneralSetting][MaxOffers]; ?> Angeboten verfügbar.&nbsp;</span>


                        </div> 
                    </div>
                </div>   
            </div>  

        </section>   
        <!-- ./COUNTDOWN -->            

        <!-- Contact Formular -->  
        <section class="form-container">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 check-submit-form" id="form" style="padding-top: 50px; padding-bottom: 20px;">     
                        <form id="api-data-form" action="<?= $_SERVER['PHP_SELF'] ?>" method="post" accept-charset="utf-8" class="form-horizontal" role="form">
                            <input type="hidden" name="contactid" value="<?= $contactid ?>">
                            <input type="hidden" name="checksum" value="<?= $checksum ?>">
                            <input type="hidden" name="mailingid" value="<?= $mailingid ?>">

                            <!-- INPUT ANREDE FRAU & HERR -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="btn-group">
                                        <?php
                                        if ($_SESSION['data']['anrede'] === "Frau") {
                                            $frau = "checked";
                                        } else {
                                            $herr = "checked";
                                        }
                                        ?>										
                                        <label class="btn btn-outline-secondary" style="border: 1px solid transparent;">
                                            <input value="Frau" style="width: 15px; height: 15px;" type="radio" name="<?php echo $config[MailInOne][Mapping][SALUTATION]; ?>" autocomplete="off" required<?php echo $frau; ?>> Frau
                                        </label>
                                        <span class="input-group-btn" style="width:15px;"></span> 
                                        <label class="btn btn-outline-secondary" style="border: 1px solid transparent;">
                                            <input value="Herr" style="width: 15px; height: 15px;" type="radio" name="<?php echo $config[MailInOne][Mapping][SALUTATION]; ?>" autocomplete="off" <?php echo $herr ?>> 
                                            Herr
                                        </label>
                                    </div>  
                                </div>       
                            </div>

                            <!-- INPUT VORNAME -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">

                                        <input value="<?php echo ((!empty($standard_001) ? $standard_001 : $_SESSION['data']['vorname'])); ?>" class="input-fields form-control" id="" name="<?php echo $config[MailInOne][Mapping][FIRSTNAME]; ?>" placeholder="*Vorname" type="text" required 
                                               <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>      
                                    </div>
                                </div>       
                            </div>


                            <!-- INPUT NACHNAME -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">

                                        <input  value="<?php echo ((!empty($standard_002) ? $standard_002 : $_SESSION['data']['nachname'])); ?>" class="input-fields form-control" id="nachname" name="<?php echo $config[MailInOne][Mapping][LASTNAME]; ?>" placeholder="*Nachname" type="text" required
                                                <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                    </div>
                                </div>       
                            </div>


                            <!-- INPUT PLZ -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <input value="<?php echo ((!empty($standard_004) ? $standard_004 : $_SESSION['data']['plz'])); ?>"  class="input-fields form-control plz" id="plz" name="<?php echo $config[MailInOne][Mapping][ZIP]; ?>" placeholder="*PLZ" type="number"  required 
                                               <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                    </div>
                                    <span class="p-light errorplz" style="color:<?php echo $config[ErrorHandling][ErrorMsg_Color]; ?>"><?php echo $config[ErrorHandling][ErrorMsgPostalcode]; ?></span>     
                                </div>       
                            </div>

                            <!-- INPUT ORT -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <input value="<?php echo ((!empty($standard_005) ? $standard_005 : $_SESSION['data']['ort'])); ?>" class="input-fields form-control ort" id="ort" name="<?php echo $config[MailInOne][Mapping][CITY]; ?>" placeholder="*Ort" type="text"  required  
                                               <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                    </div>
                                </div>       
                            </div>

                            <!-- INPUT EMAIL -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <input value="<?php echo ((!empty($email_002) ? $email_002 : $_SESSION['data']['email'])); ?>" class="input-fields form-control email" id="email" name="<?php echo $config[MailInOne][Mapping][EMAIL]; ?>" placeholder="*E-Mail-Adresse" type="email" required 
                                               <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                    </div>
                                    <?php if (($_SESSION['error'] === "email") && !empty($_SESSION['error_msg'])) { ?>   
                                        <span class="p-light erroremail"  style="float: left; padding-bottom:15px; color:<?php echo $config[ErrorHandling][ErrorMsg_Color]; ?>"><?php echo $_SESSION['error_msg']; ?></span>
                                    <?php } ?>  										
                                </div>       
                            </div>

                            <!-- INPUT TELEFON -->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <input value="<?php echo ((!empty($custom_003) ? $custom_003 : $_SESSION['data'][$config[MailInOne][Mapping][Telefon]])); ?>" class="input-fields form-control" id="telefon"  placeholder="*Telefonnummer" type="text" required <? echo ($config[GeneralSetting][CutCopyandPaste]) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?> >

                                               <input value="<?php echo $_SESSION['data'][$config[MailInOne][Mapping][Telefon]] ?>" class="input-fields form-control telefon" id="tel2" name="<?php echo $config[MailInOne][Mapping][Telefon]; ?>" placeholder="*Telefonnummer" type="text" hidden >   

                                    </div>
                                    <span class="p-light errortelefon" style="color:<?php echo $config[ErrorHandling][ErrorMsg_Color]; ?> ;float:left"><?php echo $config[ErrorHandling][ErrorMsgTelefon]; ?></span>
                                </div>       
                            </div>                                

                            <label class="form-group">
                                <div class="col-sm-12">
                                    <input style="width: 20px; height: 20px;" type="checkbox" required>
                                    <span class="p-dark">&nbsp;&nbsp;Ich bin einverstanden, dass die Schwa-Medico Medizinische Apparate Vertriebsgesellschaft mbH, Gehrnstraße 4, 35630 Ehringhausen, meine eingetragenen Daten nutzt und mir E-Mails zuschickt bzw. mich anruft, um mir Angebote aus dem Bereich EMS-Training (Zubehör, Training, Fachliteratur) zukommen zu lassen. Diese Einwilligung kann ich jederzeit widerrufen, etwa durch einen Brief an die oben genannte Adresse oder durch eine E-Mail an <a href="mailto:widerruf@schwa-medico.de">widerruf@schwa-medico.de</a>. Anschließend wird jede werbliche Nutzung unterbleiben.</span>
                                </div>    
                            </label>
                            <!--Hidden Fields-->   
                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">URL</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][url]; ?>" value="<?php echo $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>">
                                </div>
                            </div>                                        

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Source</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_source]; ?>" value="<?php echo $_GET['utm_source'] ?>">
                                </div>
                            </div>  

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Name</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_name]; ?>" value="<?php echo $_GET['utm_name'] ?>">
                                </div>
                            </div>      

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Term</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_term]; ?>" value="<?php echo $_GET['utm_term'] ?>">
                                </div>
                            </div>                             

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Content</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_content]; ?>" value="<?php echo $_GET['utm_content'] ?>">
                                </div>
                            </div>                              

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Medium</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_medium]; ?>" value="<?php echo $_GET['utm_medium'] ?>">
                                </div>
                            </div>    

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">UTM Campaign</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][utm_campaign]; ?>" value="<?php echo $_GET['utm_campaign'] ?>">
                                </div>
                            </div>                                               

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">TRAFFICSOURCE</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][trafficsource]; ?>" id="ts" value="<?php echo $_GET['trafficsource'] ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Quelle</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][Quelle]; ?>" id="quelle" value="<?php echo $config[MailInOne][Constants][Quelle]; ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Typ</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][Typ]; ?>" id="" value="<?php echo $config[MailInOne][Constants][Typ]; ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Segment</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="<?php echo $config[MailInOne][Mapping][Segment]; ?>" id="" value="<?php echo $config[MailInOne][Constants][Segment]; ?>">
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Test Mode</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="test_mode" id="" value="<?php echo $_GET['testmode']; ?>">
                                </div>
                            </div>

                            <div class="form-group"> 
                                <div class="col-sm-12">
                                    <input style="margin-top: 25px; cursor: pointer; background-color: rgb(0, 162, 219); border-color: rgb(0, 162, 219);" type="submit" class="col-sm-12 btn btn-danger btn-xl js-scroll-trigger" value="SUBMIT" name="submit" style="width: 100%;">
                                </div>        
                            </div>
                            <?php if (isset($response) && $response->isSuccess()) { ?>
                                <div class="alert alert-success fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Subscription successful</strong>
                                </div>
                            <?php } elseif (isset($warning)) { ?>
                                <div class="alert alert-warning fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong style="color: red; z-index: 1000;">Subscription failed</strong>
                                    <?= $warning['message'] ?>
                                </div>
                            <?php } elseif (isset($response)) { ?>
                                <div class="alert alert-danger fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Subscription failed</strong>
                                </div>
                            <?php } ?>
                        </form>          


                    </div>
                </div>
            </div>  
        </section>

        <!-- FOOTER SECTION -->  



        <!--script src="libraries/ini-master/ini.js"></script-->  
        <!--script src="jquery/campaign.js"></script-->  



    </body>
</html>