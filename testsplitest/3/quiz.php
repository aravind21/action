<?php
    require "vendor/autoload.php";
    require "clients/autoload.php";

    // Get the quiz questions
    spl_autoload_register('Autoload::quizLoader');

    $q = new quiz();
    $quiz = $q->getQuiz();
    
?> 
<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Nutzen Sie unser neues FREE EMS® PROGRAMM und trainieren sie bequem von zu Hause aus.">
        <meta name="author" content="StimaWELL">

        <title>Quiz</title>

        <link href="css/creative.css" rel="stylesheet" type="text/css">  
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">    
        <link rel="stylesheet" href="vendor/telefonvalidator-client/build/css/intlTelInput.css"/>
        <link rel="stylesheet" href="vendor/bxslider/src/css/jquery.bxslider.css"/>
        <link rel="stylesheet" href="css/animate.css"/>
    </head>

    <body>
        <section class="form-container quiz">
            <div class="container">
                <form id="api-data-form" action="score.php"  method="post" accept-charset="utf-8" class="form-horizontal" role="form">
                    <div class="row">
                        <div class=" bxslider">
                            <?php
                                foreach ($quiz as $q) {
                                    echo $q;
                                }
                            ?>
                        </div>
                        <div class="container col-lg-3"><button type="button" class="btn btn-success previous-slide" id="previous-slide">Previous Question</button></div>          
                        <div class="container col-lg-3"><button value="SUBMIT" name="submit" class="btn btn-success quiz-submit">Submit Quiz</button></div>
                        <div class="container col-lg-3"><button type="button" class="btn btn-success next-slide" id="next-slide">Next Question</button></div>
                    </div>
                </form>
            </div>
        </section>

        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="vendor/bxslider/src/js/jquery.bxslider.js "></script>	

        
        <script type="text/javascript ">
            $(document ).ready(function() {
                
                // Initiate the Slider
                var slider = $('.bxslider').bxSlider({

                    controls:false,
                    infiniteLoop:false,
                    touchEnabled:false,
                    pager:false
                });       
                
                // Get the slide count
                var slide_count = slider.getSlideCount();
                
                // Go to the Next slide
                $('.next-slide').click(function (){
                    slider.goToNextSlide();
                    checkSliderVzButton();
                });
                
                // Go To previous Slide 
                $('.previous-slide').click(function (e){
                    slider.goToPrevSlide();
                    checkSliderVzButton();
                });
                
                // Onload the Previous question should be disabled.
                if(slider.getCurrentSlide() <= 0){
                     $('#previous-slide').attr('disabled', true);
                }
                
                // Check the slide count and disable the previous and next slide accondingly
                function checkSliderVzButton(){
                    
                    if(slider.getCurrentSlide() <= 0){
                       $('#previous-slide').attr('disabled', true);
                    }else{
                        $('#previous-slide').attr('disabled', false);
                    }

                    if(slider.getCurrentSlide() == (slide_count - 1)){                   
                        $('#next-slide').attr('disabled', true);
                    }else{
                         $('#next-slide').attr('disabled', false);
                    }                     
                }
              
                
                console.log(slider.getCurrentSlide());
            });
        </script>
    </body>