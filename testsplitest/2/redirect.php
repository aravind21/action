<?php
// Get the config data from config.ini       
$config = parse_ini_file("config.ini", true);
$currentUrl = parse_url($_SERVER['REQUEST_URI']);
$redirect_path = $config[GeneralSetting][Redirect][RedirectPath];

if ($config[GeneralSetting][KeepURLParameter]) {
    $redirect_path = $redirect_path . $currentUrl["query"];
}
$redirect_time = $config[GeneralSetting][Redirect][RedirectTime];

// Webgains
$webgains = false;
if ($_GET['trafficsource'] === "webgains") {
    $lead_reference = $_GET['lead_reference'];
    $webgains = true;
}
?>
<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">  
        <?php if ($config[GeneralSetting][Redirect][RedirectStatus]) { ?>  
            <meta http-equiv="refresh" content="<?php echo $redirect_time; ?>;url=<?php echo $redirect_path; ?>" />  
        <?php } ?>

        <title>Wellness mit StimaWELL&reg; FaceTime - Jetzt kaufen!</title>

        <!-- implementation bootstrap -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- implementation fontawesome icons -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- implementation simpleline icons -->
        <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
        <!-- implementation googlefonts -->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
        <!-- implementation Animated Header -->
        <!-- implementation custom css -->
        <link href="css/creative.css" rel="stylesheet">
        <!-- implementation animate css -->
        <link href="css/animate.css" rel="stylesheet">
    </head>
    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s)
        {
            if (f.fbq)
                return;
            n = f.fbq = function () {
                n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq)
                f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
                'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '2069635113314986');
        fbq('track', 'PageView');
    </script>

    <noscript>
    <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=2069635113314986&ev=PageView&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->
    
    <!-- Outbrain Tracking -->
    <?php if ($config[TrackingTools][EnableOutbrain]) { ?>
        <script data-obct type="text/javascript">
            /** DO NOT MODIFY THIS CODE**/
            !function (_window, _document) {
                var OB_ADV_ID = '007035eac06ecc119b917903a5dc025178';
                if (_window.obApi) {
                    var toArray = function (object) {
                        return Object.prototype.toString.call(object) === '[object Array]' ? object : [object];
                    };
                    _window.obApi.marketerId = toArray(_window.obApi.marketerId).concat(toArray(OB_ADV_ID));
                    return;
                }
                var api = _window.obApi = function () {
                    api.dispatch ? api.dispatch.apply(api, arguments) : api.queue.push(arguments);
                };
                api.version = '1.1';
                api.loaded = true;
                api.marketerId = OB_ADV_ID;
                api.queue = [];
                var tag = _document.createElement('script');
                tag.async = true;
                tag.src = '//amplify.outbrain.com/cp/obtp.js';
                tag.type = 'text/javascript';
                var script = _document.getElementsByTagName('script')[0];
                script.parentNode.insertBefore(tag, script);
            }(window, document);
            obApi('track', 'PAGE_VIEW');
        </script>    
    <?php } ?>    
    <!-- End of Outbrain Tracking -->
    
    <body>
        <!--Adeblo Tracking-->
        <?php
        if ($config[TrackingTools][EnableAdeblo]) {
            $src = "https://adeblo.com/tracker/loader?rid=5&ord=" . $_GET['lead_reference'] . "&rev=1450";
            ?>
            <img src="<?php echo $src ?>" width="3" height="3">
        <?php } ?>  
        <!--End of Adeblo Tracking-->

        <p>&nbsp;</p>
        <div class="col-sm-12 text-center logo">
            <img class="img-fluid" alt="StimaWELL FaceTime" src="img/facetime-logo.png">
        </div>

        <!-- DESKTOP NAV -->
        <nav class="ft-nav">   
            <div class="container">   
                <div class="col-sm-12 text-center">
                    <a href="https://online-mehr-geschaeft.de/stimawell/facetime-neu/" class="js-btn" data-hover="home">stimawell&reg;</a>
                    <a href="#ueber" class="js-btn" data-hover="snippets">über FaceTime</a>
                    <a href="#anwendung" class="js-btn" data-hover="stats">anwendung</a>
                    <a href="#refs" class="js-btn" data-hover="stats">referenzen</a>
                    <a href="#buy" class="js-btn" data-hover="customers">jetzt kaufen</a> 
                </div>
            </div>
        </nav>

        <!-- MOBILE IMG --> 
        <div class="facetime-img-mobile col-sm-12 text-center">
            <img class="img-fluid" src="img/facetime-model.png"/>   
        </div>

        <!-- HERO HEADER -->
        <section class="hero">
            <div class="container">
                <div class="row">




                    <div class="hero-header col-lg-12 col-sm-12">
                        <div class="hero-intro">
                            <p>&nbsp;</p>
                            <center>
                                <h3>BITTE ÜBERPRÜFE DEIN E-MAIL POSTFACH.</h3> 
                                <hr> 
                                <p style="font-size: 18px;">Wir haben dir eine E-Mail mit einem Bestätigungslink an deine genannte E-Mail-Adresse gesendet. Solltest du in den nächsten 15 Minuten keine Mail erhalten, schaue bitte auch in dein Spam oder Junk Ordner.</p>
                            </center>
                            <p>&nbsp;</p>

                        </div>  
                    </div> 

                </div>
            </div>         
        </section>  
        <!-- ./HERO HEADER -->  



        <footer class="footer text-center">
            <div class="container">
                <div class="row">


                    <div class="col-lg-12 h-100 text-center my-auto">


                        <nav class="navbar">   
                            <div class="container">
                                <div class="col-sm-12 text-center">

                                    <a href="https://www.stimawell-ems.de/" class="js-btn" data-hover="home">Shop</a>
                                    <a href="https://www.stimawell-ems.de/impressum" class="js-btn" data-hover="home">Impressum</a>
                                    <a href="https://www.stimawell-ems.de/agb" class="js-btn" data-hover="snippets">AGB</a>
                                    <a href="https://www.stimawell-ems.de/datenschutz" class="js-btn" data-hover="features">Datenschutz</a>

                                </div>
                            </div>
                        </nav>





 <!--<p class="text-muted small mb-4 mb-lg-0"><strong>✔</strong> 40 JAHRE MADE IN GERMANY&nbsp;&nbsp;&nbsp;<strong>✔</strong> DIREKT VOM HERSTELLER&nbsp;&nbsp;&nbsp;<strong>✔</strong> SERVICE: 06443 4369914</p>-->


                        <p>&nbsp;</p>

                        <ul class="list-inline mb-0">
                            <li class="list-inline-item mr-3">
                                <a href="https://www.facebook.com/stimawellems/" target="_blank">
                                    <i class="fa fa-facebook fa-2x fa-fw"></i>
                                </a>
                            </li>
                            <li class="list-inline-item mr-3">
                                <a href="https://www.instagram.com/stimawell.ems/" target="_blank">
                                    <i class="fa fa-instagram fa-2x fa-fw"></i>
                                </a>
                            </li>
                        </ul>


                    </div> 
                </div>   
            </div>
        </footer>

        <!--Check for WebGains tracking -->
        <?php if ($config[TrackingTools][EnableWebGains]) { ?>
            <!--Web Tracking-->
            <script>
                (function (w, e, b, g, a, i, n, s) {
                    w['ITCLKOBJ'] = a;
                    w[a] = w[a] || function () {
                        (w[a].q = w[a].q || []).push(arguments)
                    }, w[a].l = 1 * new Date();
                    i = e.createElement(b), n = e.getElementsByTagName(b)[0];
                    i.async = 1;
                    i.src = g;
                    n.parentNode.insertBefore(i, n)
                })(window, document, 'script', 'https://analytics.webgains.io/clk.min.js', 'ITCLKQ');
                ITCLKQ('set', 'internal.cookie', true);
                ITCLKQ('click');
            </script>  

            <!-- Read the Webgains cookie -->
            <script type="text/javascript ">

                // Function to read the cookie
                function readCookie(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for(var i=0;i < ca.length;i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1,c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
                }
                return null;
                }  

                // Check the status of the source 
                var status = "<?= $webgains ?>";
                var lead_ref = "";

                // Check if the status is true and Webgains cookie is set
                if (readCookie('webgains') && status) {
                lead_ref = "<?= $lead_reference ?>";
                }       

            </script>

            <!-- <Webgains Tracking Code> -->
            <!-- <Variablendefinitionen> -->
            <script language="javascript" type="text/javascript">

                var wgOrderReference = lead_ref;
                var wgOrderValue = "0";
                var wgEventID = 1037645;
                var wgComment = "";
                var wgLang = "de_DE";
                var wgsLang = "javascript-client";
                var wgVersion = "1.2";
                var wgProgramID = 269785;
                var wgSubDomain = "track";
                var wgCheckSum = "";
                var wgItems = "";
                var wgVoucherCode = "";
                var wgCustomerID = "";
                var wgCurrency = "EUR";

            </script>
            <!-- </Variablendefinitionen> -->


            <!-- <Webgains Tracking Code NG> -->
            <script language="javascript" type="text/javascript">
                (function (w, e, b, g, a, i, n, s) {
                    w['ITCVROBJ'] = a;
                    w[a] = w[a] || function () {
                        (w[a].q = w[a].q || []).push(arguments)
                    }, w[a].l = 1 * new Date();
                    i = e.createElement(b),
                            n = e.getElementsByTagName(b)[0];
                    i.async = 1;
                    i.src = g;
                    n.parentNode.insertBefore(i, n)
                })(window, document, 'script', 'https://analytics.webgains.io/cvr.min.js', 'ITCVRQ');
                ITCVRQ('set', 'trk.programId', wgProgramID);
                ITCVRQ('set', 'cvr', {
                    value: wgOrderValue,
                    currency: wgCurrency,
                    language: wgLang,
                    eventId: wgEventID,
                    orderReference: wgOrderReference,
                    comment: wgComment,
                    multiple: '',
                    checksum: '',
                    items: wgItems,
                    customerId: wgCustomerID,
                    voucherId: wgVoucherCode
                });
                ITCVRQ('conversion');
            </script>

        <?php } ?>        
        <!--Check for WebGains tracking -->
        <!-- </Webgains Tracking Code NG> -->
        <!-- </Webgains Tracking Code> -->    


        <!-- Bootstrap core JavaScript -->
        <script src="vendor/jquery/jquery.min.js">
        </script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js">
        </script>
        <script src="vendor/header-animation/demo-1.js">
        </script>  
        <script src="vendor/header-animation/TweenLite.min.js">
        </script>
        <script src="vendor/header-animation/EasePack.min.js">
        </script>
        <script src="vendor/header-animation/rAF.js">
        </script>
        <script src="vendor/header-animation/demo-1.js">
        </script>  

    </body>

</html>