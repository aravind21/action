<?php

/**
 * Description of email checker
 *
 * @author Pradeep
 * 
 * Purpose: Validates the email given in the form.
 * 
 */

class emailValidatorApiClient {
        //put your code here
    var $email;
    var $user = "livetreaction";
    var $pass = "HPb0oU1rESV7";
    var $server = "https://adc.maileon.com";
    var $url = "https://adc.maileon.com/svc/2.0/address/quality";
    var $responce = array();
    var $responceSize = 0;
 

    function __construct($mail){
        $this->email = $mail;
    }
    public function curlRequest(){
        $url = $this->url . "/".$this->email;
        $ch = curl_init();
        $optArray = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array("Accept: application/json"),
            CURLOPT_USERPWD => "$this->user:$this->pass"
        );
        curl_setopt_array($ch, $optArray);
        $responce = curl_exec($ch);
        $info = curl_getinfo($ch);
        $close = curl_close($ch);
        $this->responce = json_decode($responce, true);
        if(!(isset($this->responce) && $close)){
            return false;
        }
        return true;
    }
    
    public function curlResponce(){
        if(isset($this->responce)) {
            $this->responceSize = sizeof($this->responce);
        }
        $result = $this->parseResponce();
        return $result;
    }
    
    private function parseResponce(){
        $result = true; 
		#var_dump($this->responce);
        foreach( $this->responce as $key => $val){
            switch ($key){
                case "address":
                    if($val == 0){
                        $result = false;
                    }
                    break;
                case "mailserver":
                    if($val == 0){
                        $result = false;
                    }
                    break;  
            }
        }
        return $result;  
    }
}
